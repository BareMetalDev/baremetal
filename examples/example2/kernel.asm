;------------------------------- Start of Kernel Code ------------------------------


ORG 0x8000   ; Set origin to 0x8000 where this will be loaded
BITS 16      ; Impose real mode

jmp main_kernel         ; Jump to main kernel code
%include "print.asm"    ; Inline include for print functions


main_kernel:            ; Main Kernel entry point

    mov si,welcome_from_kernel  ; Print Welcome message defined below
    call bios_print

halt:                   ; Halt the Kernel
    jmp halt

welcome_from_kernel   db 'Welcome Bare Metal Team From Kernel Land', 13, 10, 0

;-----------------------------------------------------------------------------------
