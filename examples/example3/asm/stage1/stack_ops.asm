;----------------------push_segment_index Macro-----------------------;
; This macro pushs the eflags, DS, ES, DI, SI regsiters to the stack
;----------------------------------------------------------------------;

%macro push_segment_index 0
    pushf
    push ds
    push es
    push di
    push si

%endmacro

;----------------------push_segment_index Macro-----------------------;
; This macro pops the eflags, DS, ES, DI, SI regsiters from the stack
;----------------------------------------------------------------------;

%macro pop_segment_index 0
    pop si
    pop di
    pop es
    pop ds
    popf

%endmacro
