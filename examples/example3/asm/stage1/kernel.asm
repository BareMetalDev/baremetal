;------------------------------- Start of Staget 1 Kernel Code ------------------------------
ORG 0x8000   ; Set origin to 0x8000 where this will be loaded
BITS 16      ; Impose real mode

start_start:

jmp main_kernel         ; Jump to main kernel code
%include "stack_ops.asm"    ; Inline include for print functions
%include "print.asm"    ; Inline include for print functions
%include "a20.asm"      ; Inline include for A20 Detection and enablement functions
%include "check_cpu.asm"    ; Inline include for checking if CPU supports Long MOde
%include "memory_scanner.asm"    ; Inline code for memory detection function


main_kernel:            ; Main Kernel entry point

    mov si,welcome_from_kernel  ; Print Welcome message defined below
    call video_print


    xor ax, ax          ; Set AX to zero

    mov ss, ax          ; Set Stack Segment to 0

    mov sp, start_start ; Set the stack pointer to start just right below this code
                        ; Note: The stack grows upwards

    mov ds, ax          ; Set DS to zero
    mov es, ax          ; Set ES to zero
    mov fs, ax          ; Set FS to zero
    mov gs, ax          ; Set GS to zero

    cld                 ; Clear Direction Flag

    call CheckA20       ; Check if the A20 Line address is enabled

    cmp byte [a20_enabled],0x01 ; If enabled skip enabling it
    je skip_enable_a20          ; Else call EnableA20
        call EnableA20
    skip_enable_a20:            ; Skipping EnableA20
        call CheckCPU           ; Check If CPU supports Long Mode
        mov di,memory_info      ; Set DI register to point to memory location
                                ; memory_info to store mory entries into
        call memory_scanner     ; Call memory_scanner to detect memory entries


halt:                   ; Halt the Kernel
    jmp halt            ; Infinite for loop

; Welcome message to be printed upon entry to the Kernel first stage
welcome_from_kernel   db 'Welcome Bare Metal Team From Kernel Land', 13, 10, 0

;-----------------------------------------------------------------------------------

; Pad out file to 4K.
times 4096 - ($-$$) db 0
