;-------------------------------- Boot Sector ------------------------

[ORG 0x7e00]   ; Set load address to 0x7c00
   xor ax, ax  ; Make it zero
   mov ds, ax  ; Set data segment to zero

   jmp load_kernel   ; jump to load kernel

%include "print.asm"    ; Inline include of of Print Functions
%include "disk.asm"     ; Inline include of Disk Functions

load_kernel:
        call bios_cls           ; Clear screen
        mov si, welcome_msg     ; Print welcome message
        call bios_print
     	call get_boot_drive     ; Identify the boot device and save
                                ; its number in variable boot_drive
; We start raeding with preset values of number of sector 16,
; and memory addres to read data to is 0x8000

    disk_read_loop:             ; Read Sectors loop
        mov ax,[sectors_to_read]; Check if finished reading
        cmp ax,0x0
        je disk_read_loop_done  ; If so exit loop
        call read_sector        ; Else read current sector
                                ; and increment disk and memory
    jmp disk_read_loop          ; Loop
    disk_read_loop_done:        ; Exit loop label

    mov ax,0x0                  ; Check if finished reading
    mov al,[read_finish]        ; through checing th read_finis
    cmp ax,0x1                  ; flag
    je finished_load


    mov ax,0x1                  ; Set the read_finish_flag to terminate
    mov [read_finish],al        ; after next eteration
    mov ax,0x100                ; Set number to sectors to read to 256
    mov [sectors_to_read],ax
    mov ax,0x1000               ; set read memory segment 0x1000
    mov [read_segment],ax
    mov ax,0x0                  ; Set memory address to zero
    mov [read_address],ax       ; Effective address 0x1000:0000 = 0x10000
    jmp disk_read_loop          ; Loop one more time



finished_load:                  ; Finished reading sectors
    mov si,newline              ; Print new line
    call bios_print
    mov si,done_read_msg        ; Print completion message
    call bios_print
    jmp 0x8000                  ; Jump to where we loaded the kernel,
                                ; offset kernel_segments
halt:
        jmp halt

%include "data.asm"

times 512-($-$$) db 0 ; Pad to 510 bytes

