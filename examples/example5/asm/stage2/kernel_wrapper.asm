[BITS 64]
SECTION .text           ; Text section contains machine instructions
                        ; Basically code segment
global _start           ; Define _start label as global to be seen by
                        ; the linker file
_start:                 ; Entry point defined in the linker file
jmp Main64              ; Jump to Main entry point label

Main64:
	cli                           ; Clear the interrupt flag.
                                  ; Disable Intrrupta
    mov rsp,_stack_start          ; Move stack start inro stack pointer
    extern kernel_main            ; Define an external symbol for the kernel main
                                  ; function defined in the C source
    call kernel_main              ; Call Kernel main C function

khalt:                            ; Halt loop
    hlt
    jmp khalt


SECTION .bss            ; BSS section conatines uninitialized global
                        ; or static variables
_stack_end:             ; Label defining the end of the stack
    resb    8192        ; reserve 8 KB
_stack_start:           ; Label defining the start of the stack
