hexa2ascii:  ;AX input , si point result storage address
    mov dx,16
    mov bx,ax
    next_digit:
        mov ax,bx
        cmp dl,0x0
        je hexa2ascii_done
        mov cx,dx
        sub cx,4
        shr ax,cl
        and al,0x0f
        cmp al,0x9
        jle  less_than_9
        sub al,0xa
        add al,'A'
        jmp store_digit
    less_than_9:
        add al,'0'
    store_digit:
        mov [si],al
        add si,0x1
        sub dx,0x4
        jmp next_digit
    hexa2ascii_done:
ret
