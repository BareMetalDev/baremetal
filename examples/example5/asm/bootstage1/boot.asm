;-------------------------------- Boot Sector ------------------------

[ORG 0x7c00]   ; Set load address to 0x7c00
    xor ax, ax  ; Make it zero
    mov ds, ax  ; Set data segment to zero
    mov ss, ax  ; Set data segment to zero
    mov sp, 0x7c00

    mov ah, 0x0                    ; RESET-command
    int 13h                        ; Call interrupt 13h
    push dx
	mov si,boot_from_msg
    call bios_print
    pop dx
    xor ax,ax
    mov al,dl

	mov si,hexa_string
	call hexa2ascii
	mov si,hexa_string
	call bios_print
	mov si,newline
	call bios_print



    mov ax, 0x0      ; Load read segment into ES
    mov es, ax
    mov bx, 0x7e00      ; Destination address
    mov ah, 0x2                 ; READ SECTOR-command
    mov al, 0x1                 ; Number of sectors to read (1 Sector)
    mov ch, 0x0          ; Cylinder
    mov cl, 0x2          ; Starting Sector
    mov dh, 0x0         ; Head
    int 0x13                    ; Call interrupt 13h


   jmp 0x7e00   ; jump to load kernel
hlt:
    jmp hlt

%include "print.asm"    ; Inline include of of Print Functions
%include "hexa2ascii.asm"    ; Inline include of of Print Functions

boot_from_msg db  "Booting from: ",0
newline db "xxxx",13,10,0
hexa_string  times 10 db 0

times 446-($-$$) db 0   ; Padding to make the MBR 512 bytes

; Hardcoded partition entries
part_boot:
    db 0x80 ; Bootable and Active
    db 0x00 ; Starting Heads
    db 0x01 ; Starting Sector
    db 0x00 ; Starting Cylender
    db 0x83 ; Linux Partition
    db 0x80 ; 128 Heads
    db 10111111b ; 63 sectors
    db 00000000b ; 1024 Cylendars
    dd 0x01 ; Starting Sector LBA
    dd 0xF4200 ; Number of sectors in partition


;    dw 0x0180, 0x0001, 0xFE83, 0x3c3f, 0x003F, 0x0000, 0xF3BE, 0x000E
part_sda2:
    dw 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
;    dw 0x0000, 0x3D01, 0xFE83, 0xFFFF, 0xF3FD, 0x000E, 0x5AF0, 0x01B3
part_sda3:
    dw 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
;    dw 0xFE00, 0xFFFF, 0xFE83, 0xFFFF, 0x4EED, 0x01C2, 0xb113, 0x001D
part_sda4:
    dw 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000

db 0x55         ; Boot Seactor signature
db 0xAA

