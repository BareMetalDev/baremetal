[ORG 0x7c00]   ; Set load address to 0x7c00


   xor ax, ax  ; Make it zero
   mov ds, ax

   mov si, msg
   call bios_print

hang:
   jmp hang

msg   db 'Hello Bare Metal Team -:)', 13, 10, 0

bios_print:
   xor ax,ax
   lodsb      ; Load byte from si to al and incremenet al
   or al, al  ; zero=end of str
   jz done    ; Get out
   mov ah, 0x0E ; Print character function
   int 0x10   ; Print character loaded in al
   jmp bios_print ; Loop to next character
done:
   ret		; End Subroutine

   times 510-($-$$) db 0 ; Pad to 510 bytes
   db 0x55		 ; Boot Seactor signature
   db 0xAA

