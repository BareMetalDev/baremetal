#include "includes.h"
#include "defines.h"

/**************************************************************************
                        Video Parameters
**************************************************************************/

volatile uint16_t * terminal_buffer=NULL; // Buffer pointing to the video RAM
uint8_t x_position;             // The current screen horizontal position
uint8_t y_position;             // The current screen vertical position



/**************************************************************************
                        Video Function Definitions
**************************************************************************/

void video_initialize();        // Initializes video variables
// Display an ascii character at a specific position on the screen
void video_putCharacterAt (char c, unsigned char x_pos, unsigned char y_pos, uint8_t backgroundColour, uint8_t forgroundColour);
// Display a null terminated string on the screen
void video_putString (const char* buffer, uint8_t backgroundColour, uint8_t forgroundColour );
// Display a an ascii characters on the current position on the screen
void video_putCharacter (char c, uint8_t backgroundColour, uint8_t forgroundColour);


/**************************************************************************
                        Video Function Definitions
**************************************************************************/


/**************************************************************************
                        video_initialize
Intializes the terminal_buffer variable to the video ram address
set the horizontal position to location 20 and vertical position to location
30 to start displaying relatively in the middle of the screen.
**************************************************************************/
void video_initialize()
{
        terminal_buffer = (volatile uint16_t*) 0x0B8000;
        x_position = 20;
        y_position = 15;

}

/**************************************************************************
                        video_putCharacterAt
Displays a character at a specific position on the screen, with specific
color attributes. Each character on the screen is represented by 2 bytes
the first for the colors and the second is for the video_putCharacterAt.
The higher 4 bits of the first byte are for the background and the lower 4
bits are for the foreground
**************************************************************************/

void video_putCharacterAt (char c, unsigned char x_pos, unsigned char y_pos, uint8_t backgroundColour, uint8_t forgroundColour)
{
    /* Shift backgound 4 bits and or it with the for ground and then shift it 8
        to make space for the ascii character to be stored in the lower byte
    */
    uint16_t color16 = (forgroundColour | backgroundColour << 4) << 8;
    // calculate the current location in the buffer. We use volatile to avoid caching
    volatile uint16_t * location = terminal_buffer + ((y_pos* VGA_MAX_X) + x_pos);
    // OR the color and the ascii character and store it in the video ram
    *location = c | color16;
}
/**************************************************************************
                        video_putCharacter
Displays a character on the current location with a specific color attributes.
Basically calls video_putCharacter with the x_position and y_position values.
**************************************************************************/

void video_putCharacter (char c, uint8_t backgroundColour, uint8_t forgroundColour)
{
    video_putCharacterAt (c, x_position, y_position, backgroundColour, forgroundColour);
}

/**************************************************************************
                        video_putString
Display a NULL terminating string on the screen. It keeps on printing character
by character and advance the screen position. When the ascii character '10' is
encountered a new line constructed via increasing the y_position and setting
x_position to 0. The same takes place when the position reach the end of the
screen horizontally
**************************************************************************/

void video_putString (const char* buffer, uint8_t backgroundColour, uint8_t forgroundColour )
{
    for ( int i = 0 ; buffer[i] != 0 ; i ++) // Loop until we encounter a NULL character
    {
        if ( buffer[i] == 10)   // if character is '10' advance to next line
        {
            if ( y_position < VGA_MAX_Y) // check for end of screen vertically to avoid overflow
            {
                y_position++;
                x_position = 0;
            }
            else break; // break if on overflow condition
        }
        else
        {
            video_putCharacter(buffer[i],backgroundColour,forgroundColour); // Print chracter
            if ( x_position < VGA_MAX_X) x_position++;  // addvance horizontally if not end of the screen
            else if ( y_position < VGA_MAX_Y)   // Else go to the next lin
            {
                y_position++;
                x_position = 0;
            }
            else break;
        }
    }
}

/**************************************************************************
                        Kernel Main
The main kernel entry.
For now we print a welcome message in the middle of the screen
**************************************************************************/


extern "C" void kernel_main()
{
        video_initialize();
        video_putString("Hello Bare Metal Team from Kernel C Land\n",COLOR_RED,COLOR_WHITE);
}
